# alacritty-sonicksseven

## My setting alacritty

This project is a simple setting for my alacritty console.

But has a custom configuration and is based is this sources

- [Alcritty repository](https://github.com/alacritty/alacritty)
- [Alacritty themes](https://github.com/alacritty/alacritty-theme)
- [Congif alacritty](https://alacritty.org/config-alacritty.html)
- [Tmux keyboard shortcuts](https://tmuxcheatsheet.com/)

## file tmux.conf

First you must run this command:

`tmux show -g | cat > ~/.tmux.conf`


Now drop all content and only this file must have:

```bash
#set -g mouse on
setw -g mode-keys vi
```

And finally you must run this command and your tmux is ok:

```bash
exit tmux
$ tmux kill-server
$ tmux
``